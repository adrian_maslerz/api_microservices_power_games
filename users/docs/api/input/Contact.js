
/**
 * @api {post} /contact Send contact message
 * @apiSampleRequest /contact
 * @apiVersion 0.0.1
 * @apiName Send contact message
 * @apiGroup Contact
 *
 * @apiParam {String} email Email address
 * @apiParam {String} subject Subject. Max 200 characters.
 * @apiParam {String} message Message. Max 500 characters.
 *
 * @apiSuccess (Success 201){Boolean} status Operation status.
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 201 OK
 *     {
 *         "status": true
 *     }
 *
 * @apiUse WrongParametersError
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 406 Wrong parameters
 *     {
 *          "errors": [
 *              {
 *                   "field": "message",
 *                   "message": "Message is required"
 *              }
 *          ]
 *      }
 *
 */
