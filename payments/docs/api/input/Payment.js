/**
 * @apiDefine paymentsListResults
 * @apiSuccess {String} results._id Payment id
 * @apiSuccess {String="pln"} results.currency Payment currency
 * @apiSuccess {String} results.description Payment description
 * @apiSuccess {Number} results.amount Payment amount
 * @apiSuccess {Number} results.created Payment created date as timestamp ms.
 *
 * @apiSuccess {Object} results.customer Customer object
 * @apiSuccess {String} results.customer._id Customer id
 * @apiSuccess {Number} results.customer.created Customer created date as timestamp ms.
 * @apiSuccess {String} results.customer.stripe_id Customer stripe id
 * @apiSuccess {String} results.customer.stripe_id Customer stripe id
 * @apiSuccess {String} results.customer.user Customer user id reference
 * @apiSuccess {String} results.customer.email Customer email
 *
 * @apiSuccess {Object} results.card Card object
 * @apiSuccess {String} results.card.card_id Card id from stripe
 * @apiSuccess {String} results.card.brand Card brand
 * @apiSuccess {String} results.card.last_4 Card last 4
 * @apiSuccess {String} results.card.exp_month Card expiration month
 * @apiSuccess {String} results.card.exp_year Card expiration year
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *          "results": [
 *              {
 *                  "_id": "5f0f8ac6a7af7004227b55e2",
 *                  "created": 1594854086262,
 *                  "customer": {
 *                      "_id": "5f0e2b6b5ea2070180507458",
 *                      "created": 1594764139935,
 *                      "stripe_id": "cus_HeEgew5AczOecB",
 *                      "user": "5f07999fa53df50111178b35",
 *                      "email": "test@test.com"
 *                  },
 *                  "order": "5f07999fa53df50111178b35",
 *                  "card": {
 *                      "card_id": "card_1H5JbKIs9F98ahEkwhtVKqSq",
 *                      "brand": "Visa",
 *                      "last_4": "4242",
 *                      "exp_month": "7",
 *                      "exp_year": "2021"
 *                  },
 *                  "currency": "pln",
 *                  "description": "Payment for order 5f07999fa53df50111178b35",
 *                  "amount": 10
 *              }
 *          ],
 *          "pages": 1,
 *          "total": 3,
 *     }
 *
 **/

/**
 * @api {post} /payments Add payment
 * @apiSampleRequest /payments
 * @apiVersion 0.0.1
 * @apiName Add payment
 * @apiGroup Payment
 *
 * @apiParam {String} token Stripe card token or card id from stripe.
 * @apiParam {String} order Valid object id of order.
 * @apiParam {Number} amount Amount to pay. Min 2.
 *
 * @apiSuccess (Success 201){Boolean} status Operation status.
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 201 OK
 *     {
 *         "status": true,
 *         "_id": "5f0f8ac6a7af7004227b55e2"
 *     }
 *
 * @apiUse WrongParametersError
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 406 Wrong parameters
 *     {
 *          "errors": [
 *              {
 *                   "field": "token",
 *                   "message": "token is required"
 *              }
 *          ]
 *      }
 *
 * @apiUse ConflictError
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 409 Conflict
 *     {
 *          "errors": [
 *              {
 *                   "field": "payment",
 *                   "message": "Payment already exist"
 *              }
 *          ]
 *      }
 * @apiUse AuthenticateElements
 * @apiUse AuthorizeElements
 */

/**
 * @api {get} /payments Payments list
 * @apiSampleRequest /payments
 * @apiVersion 0.0.1
 * @apiName Payments list
 * @apiGroup Payment
 *
 * @apiUse PaginationElements
 *
 * @apiUse paymentsListResults
 *
 * @apiUse AuthenticateElements
 * @apiUse AuthorizeElements
 **/

/**
 * @api {get} /customers/:id/payments Customer payments list
 * @apiSampleRequest off
 * @apiVersion 0.0.1
 * @apiName Customer payments list
 * @apiGroup Payment
 *
 * @apiParam {String} id Customer id.
 * @apiUse PaginationElements
 * @apiUse paymentsListResults
 *
 * @apiUse NotFoundError
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not found
 *     {
 *          "errors": [
 *              {
 *                   "field": "customer",
 *                   "message": "Customer not found"
 *              }
 *          ]
 *      }
 * @apiUse AuthenticateElements
 * @apiUse AuthorizeElements
 **/

/**
 * @api {get} /payments/:id Payment details
 * @apiSampleRequest off
 * @apiVersion 0.0.1
 * @apiName Payment details
 * @apiGroup Payment
 *
 * @apiParam {String} id Payment id.
 *
 * @apiSuccess {String} _id Payment id
 * @apiSuccess {String="pln"} currency Payment currency
 * @apiSuccess {String} description Payment description
 * @apiSuccess {Number} amount Payment amount
 * @apiSuccess {Number} created Payment created date as timestamp ms.
 *
 * @apiSuccess {Object} customer Customer object
 * @apiSuccess {String} customer._id Customer id
 * @apiSuccess {Number} customer.created Customer created date as timestamp ms.
 * @apiSuccess {String} customer.stripe_id Customer stripe id
 * @apiSuccess {String} customer.stripe_id Customer stripe id
 * @apiSuccess {String} customer.user Customer user id reference
 * @apiSuccess {String} customer.email Customer email
 *
 * @apiSuccess {Object} card Card object
 * @apiSuccess {String} card.card_id Card id from stripe
 * @apiSuccess {String} card.brand Card brand
 * @apiSuccess {String} card.last_4 Card last 4
 * @apiSuccess {String} card.exp_month Card expiration month
 * @apiSuccess {String} card.exp_year Card expiration year
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *      {
 *          "_id": "5f0f8ac6a7af7004227b55e2",
 *          "created": 1594854086262,
 *          "customer": {
 *              "_id": "5f0e2b6b5ea2070180507458",
 *              "created": 1594764139935,
 *              "stripe_id": "cus_HeEgew5AczOecB",
 *              "user": "5f07999fa53df50111178b35",
 *              "email": "test@test.com"
 *          },
 *          "order": "5f07999fa53df50111178b35",
 *          "card": {
 *              "card_id": "card_1H5JbKIs9F98ahEkwhtVKqSq",
 *              "brand": "Visa",
 *              "last_4": "4242",
 *              "exp_month": "7",
 *              "exp_year": "2021"
 *          },
 *          "currency": "pln",
 *          "description": "Payment for order 5f07999fa53df50111178b35",
 *          "amount": 10
 *      }
 *
 * @apiUse NotFoundError
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not found
 *     {
 *          "errors": [
 *              {
 *                   "field": "payment",
 *                   "message": "Payment not found"
 *              }
 *          ]
 *      }
 * @apiUse AuthenticateElements
 * @apiUse AuthorizeElements
 **/
