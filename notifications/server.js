
//loading config
require('dotenv').config();

//services
const QueuesHandler = require('./services/QueuesHandler');
const MailHandler = require('./services/MailHandler');

//queues config
const config = {
    queue: "emails",
    exchange: "emails"
};

//Notifications listener
const handler = new QueuesHandler();
handler.subscribe(data => {

    console.log(data)
    switch (data.type)
    {
       case "PASSWORD_RESET":
       {
            MailHandler.passwordResetMail(data.data);
            break;
       }
       case "ACCOUNT_CREATED":
       {
           MailHandler.accountCreated(data.data);
           break;
       }
       case "CONTACT_MESSAGE":
       {
           MailHandler.contactMessage(data.data);
           break;
       }
       case "ORDER_PLACED":
       {
           MailHandler.orderPlaced(data.data);
           break;
       }
       case "GAME_CODES":
       {
           MailHandler.gameCodes(data.data);
           break;
       }
    }
}, config);
