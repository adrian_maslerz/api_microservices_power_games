
//services
const Utils = require('../services/utilities');

//models
const { Customer } = require("../models/Customer");

module.exports = class CustomersController
{
	async getCustomer(req, res)
	{
		//checking permission to authenticated user
		if(req.user._id != req.params.id)
			return res.status(403).json(Utils.parseStringError('Permission denied', 'permission'));

		//getting customer
		const customer = await Customer.findOne({ user: req.user._id }, {
			stripe_id: 1,
			user: 1,
			email: 1,
			cards: 1,
			created: 1
		})
		.lean()
		.exec()
		.catch(error => console.log("Invalid object id"))

		//not found
		if(!customer)
			return res.status(404).json(Utils.parseStringError('Customer not found', 'customer'));

		//sending response
		res.json(Customer.parse(customer))
	}
}
