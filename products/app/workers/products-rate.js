
//loading config
require('dotenv').config();
require("./../../config/database").load();

//models
const { Product } = require("./../models/Product");

//services
const QueuesHandler = require('./../services/core/QueuesHandler');

//queues config
const config = {
    queue: "product_rate",
    exchange: "product_rate"
};

//Notifications listener
const handler = new QueuesHandler();
handler.subscribe(async data => {

    console.log(data)
    if(data.rate && data.id)
    {
        const product = await Product.findById(data.id).exec().catch(error => console.log(error));
        if(product)
        {
            product.rate = data.rate;
            await product.save().catch(error => console.log(error))
        }
    }

}, config);
