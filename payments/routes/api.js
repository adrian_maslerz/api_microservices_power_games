//services
const AppRouter = require('../app/services/core/AppRouter');

//controllers
const CustomersController = require("../app/controllers/CustomersController");
const PaymentsController = require("../app/controllers/PaymentsController");
const CardsController = require("../app/controllers/CardsController");

//middlewares
const authorize = require("./../app/middlewares/authorize");

const path = "/v1";
const routes = [

	//Customers
	{
		path: "/customers", controller: CustomersController, children: [
			{ path: "/:id", method: "get", middlewares: [ authorize(["USER_FULL"]) ], function: "getCustomer" },
			{ path: "/:id/payments", controller: PaymentsController, method: "get", middlewares: [ authorize(["USER_FULL"]) ], function: "getCustomerPayments" },
		]
	},

	//Cards
	{
		path: "/cards", controller: CardsController, children: [
			{ path: "/:id", method: "delete", middlewares: [ authorize(["USER_FULL"]) ], function: "deleteCustomerCard" },
		]
	},

	//Payments
	{
		path: "/payments", controller: PaymentsController, children: [
			{ path: "/", method: "post", middlewares: [ authorize(["USER_FULL"]) ], function: "addPayment" },
			{ path: "/", method: "get", middlewares: [ authorize(["ADMIN_FULL"]) ], function: "getPayments" },
			{ path: "/:id", method: "get", middlewares: [ authorize() ], function: "getPayment" },
		]
	}
];

module.exports.load = function(app)
{
	//create routes
	let appRouter = new AppRouter();
	appRouter.create(routes);

	//documentation
	appRouter.registerDocumentation(path);

	app.use(path, appRouter.router);
}


