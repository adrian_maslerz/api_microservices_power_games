//services
const AppRouter = require('../app/services/core/AppRouter');

//controllers
const OrdersController = require("../app/controllers/OrdersController");

//middlewares
const authorize = require("./../app/middlewares/authorize");

const path = "/v1";
const routes = [

	//Orders
	{
		path: "/orders", controller: OrdersController, children: [
			{ path: "/", method: "post", middlewares: [ authorize(["USER_FULL"]) ], function: "addOrder" },
			{ path: "/", method: "get", middlewares: [ authorize() ], function: "getOrders" },
			{ path: "/reviews", method: "get", function: "getReviewsHome" },
			{ path: "/:id", method: "get", middlewares: [ authorize() ], function: "getOrder" },
			{ path: "/:order/items/:id", method: "post", middlewares: [ authorize(["USER_FULL"]) ], function: "reviewOrderProduct" },
			{ path: "/:order/items/:id", method: "put", middlewares: [ authorize(["ADMIN_FULL"]) ], function: "changeReviewState" }
		]
	}
];

module.exports.load = function(app)
{
	//create routes
	let appRouter = new AppRouter();
	appRouter.create(routes);

	//documentation
	appRouter.registerDocumentation(path);

	app.use(path, appRouter.router);
}


