//loading environment config
const env = process.env;

module.exports = {
	app: {
		name: env.APP_NAME
	},
	database: {
		host: env.DB_HOST || "localhost",
		srv: Boolean(parseInt(env.DB_SRV)),
		port: env.DB_PORT,
		user: env.DB_USER || "",
		password: env.DB_PASSWORD || "",
		database: env.DB_DATABASE
	},
	services: {
		stripe: {
			key: env.STRIPE_KEY
		}
	},
	microservices: {
		users: {
			url: env.MICROSERVICE_USERS_URL,
			prefix: "/v1"
		}
	},
	folders: {
		views: __dirname + "/../views",
		uploads: __dirname + "/../public/uploads",
		public: __dirname + "/../public",
	}
}
