//services
const AppRouter = require('../app/services/core/AppRouter');

//controllers
const AuthController = require('../app/controllers/AuthController');
const UsersController = require('../app/controllers/UsersController');
const GeneralController = require('../app/controllers/GeneralController');

//middlewares
const authenticate = require('../app/middlewares/authenticate');

const path = "/v1";
const routes = [

	//Auth
	{
		path: "/", controller: AuthController, children: [
			{ path: "/register", method: "post", function: "register"},
			{ path: "/login", method: "post", function: "login"},
			{ path: "/password/remind", method: "post", function: "remindPassword"},
			{ path: "/password/remind", method: "put", function: "remindPasswordChange"},
			{ path: "/authorize", method: "post", middlewares:[ authenticate ], function: "authorize"}
		]
	},

	//User
	{
		path: "/user", controller: UsersController, middlewares:[ authenticate ], children: [
			{ path: "/profile", method: "get", function: "profile"},
			{ path: "/profile", method: "put", function: "updateProfile"},
			{ path: "/password", method: "put", function: "changePassword"},
			{ path: "/email", method: "put", function: "changeEmail"}
		]
	},

	//General
	{
		path: "/contact", controller: GeneralController, method: "post", function: "contact"
	}
];

module.exports.load = function(app)
{
	//create routes
	let appRouter = new AppRouter();
	appRouter.create(routes);

	//documentation
	appRouter.registerDocumentation(path);

	app.use(path, appRouter.router);
}


