// core
const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');
const mongooseAggregatePaginate = require('mongoose-aggregate-paginate');
const uniqueValidator = require('mongoose-unique-validator');

// schema
const schema = mongoose.Schema({
    customer: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Customer',
        required: [true, '{PATH} is required.'],
    },
    order: {
        type: mongoose.Schema.Types.ObjectId,
        required: [true, '{PATH} is required.'],
    },
    card: {
        card_id: {
            type: String,
            required: [true, '{PATH} is required.'],
        },
        brand: {
            type: String,
            required: [true, '{PATH} is required.'],
        },
        last_4: {
            type: String,
            required: [true, '{PATH} is required.'],
        },
        exp_month: {
            type: String,
            required: [true, '{PATH} is required.'],
        },
        exp_year: {
            type: String,
            required: [true, '{PATH} is required.'],
        }
    },
    currency: {
        type: String,
        required: [true, '{PATH} is required.'],
        enum: ["pln"]
    },
    description: {
        type: String,
        required: [true, '{PATH} is required.']
    },
    amount: {
        type: Number,
        required: [true, '{PATH} is required.'],
        min: [0, '{PATH} cannot be lower than {MIN}.']
    },
    created: {
        type: Date,
        default: Date.now()
    },
    updated: {
        type: Date,
        default: Date.now()
    }
}, { usePushEach: true }).index({ customer: 1, order: 1}, { unique: true });

// middlewares
schema.pre('save', function (next) {

    // dates handle
    const payment = this;
    if (payment.isNew)
        payment.created = new Date();
    if (payment.isModified())
        payment.updated = new Date();

    next();
});

// statics
schema.statics.parse = function (payment, req)
{
    //dates
    if (payment.created)
        payment.created = payment.created.getTime();

    //customer
    if(payment.customer)
    {
        const { Customer } = require("./Customer");
        payment.customer = Customer.parse(payment.customer, req);
    }

    return payment;
};

schema.plugin(uniqueValidator, {message: 'The {PATH} has already been taken.'});
schema.plugin(mongoosePaginate);
schema.plugin(mongooseAggregatePaginate);

module.exports = { Payment: mongoose.model('Payment', schema) };
