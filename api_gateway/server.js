//core
const gateway = require('fast-gateway')
const rateLimit = require('express-rate-limit')
const requestIp = require('request-ip')

//config
require('dotenv').config();
const variables = require("./variables");

const server = gateway({
	middlewares: [
		(req, res, next) => {
			req.ip = requestIp.getClientIp(req)
			return next()
		},
		rateLimit({
			windowMs: 1 * 60 * 1000, // 1 minute
			max: 100, // limit each IP to 100 requests per windowMs
			keyGenerator: req => req.headers["x-real-ip"],
			handler: (req, res) => res.send({ errors: [ { field: "general", message: "Too many requests, please try again later."}] }, 429)
		})
	],
	routes: [
		...Object.keys(variables.microservices).map(key => { return { prefix: variables.microservices[key].prefix, target: variables.microservices[key].url} }),
		...[{
			prefix: "/*",
			middlewares: [
				(req, res, next) => res.send({ errors: [ { field: "general", message: "Page not found"}] }, 404)
			]
		}]
	],
})

server.start(variables.app.port || 3000)
