//services
const AppRouter = require('../app/services/core/AppRouter');

//controllers
const CategoriesController = require("./../app/controllers/CategoriesController");
const ProductsController = require("./../app/controllers/ProductsController");

//middlewares
const authorize = require("./../app/middlewares/authorize");

const path = "/v1";
const routes = [

	//Categories
	{
		path: "/categories", controller: CategoriesController, children: [
			{ path: "/", method: "get", function: "getCategories" },
			{ path: "/:id", method: "get", function: "getCategory" },
			{ path: "/", method: "post", middlewares: [ authorize(["ADMIN_FULL"]) ], function: "addCategory" },
			{ path: "/:id", method: "put", middlewares: [ authorize(["ADMIN_FULL"]) ], function: "updateCategory" },
			{ path: "/:id", method: "delete", middlewares: [ authorize(["ADMIN_FULL"]) ], function: "deleteCategory" },
		]
	},

	//Products
	{
		path: "/products", controller: ProductsController, children: [
			{ path: "/", method: "get", function: "getProducts" },
			{ path: "/", method: "post", middlewares: [ authorize(["ADMIN_FULL"]) ], function: "addProduct" },
			{ path: "/home", method: "get", function: "getHomeProducts" },
			{ path: "/:id", method: "get", function: "getProduct" },
			{ path: "/:id", method: "put", middlewares: [ authorize(["ADMIN_FULL"]) ], function: "updateProduct" },
			{ path: "/:id", method: "delete", middlewares: [ authorize(["ADMIN_FULL"]) ], function: "deleteProduct" },
			{ path: "/photos", method: "post", middlewares: [ authorize(["ADMIN_FULL"]) ], function: "uploadPhoto" }
		]
	}
];

module.exports.load = function(app)
{
	//create routes
	let appRouter = new AppRouter();
	appRouter.create(routes);

	//documentation
	appRouter.registerDocumentation(path);

	app.use(path, appRouter.router);
}


