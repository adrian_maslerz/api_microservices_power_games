
const prepareValidInputNumberFunction = (input) => {
    return Math.max(parseInt(input) || 0, 0);
}

module.exports = {
    prepareValidInputNumber: prepareValidInputNumberFunction,

    prepareNumberRageForField: (params, fieldName, pipelineQuery = true) => {

        let start = prepareValidInputNumberFunction(params[fieldName + "_from"]);
        let end = prepareValidInputNumberFunction(params[fieldName + "_to"]);

        //ensure start is greater than end
        if(start && end && start > end)
        {
            const temp = end;
            end = start;
            start = temp;
        }

        if(!pipelineQuery)
            return { start, end }

        return [
            ...start ? [{ [fieldName]: { $gte: start } }] : [],
            ...end ? [{ [fieldName]: { $lte: end } }] : [],
        ]
    }
}

