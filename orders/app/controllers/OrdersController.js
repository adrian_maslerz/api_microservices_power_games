//core
const mongoose = require("mongoose");
const randomstring = require("randomstring");

//services
const Utils = require('../services/utilities');
const FileHandler = require("./../services/core/FileHandler");
const HttpClient = require("./../services/core/HttpClient");
const OrdersQueryHandler = require("./../services/repositories/OrdersQueryHandler");

//config
const variables = require("./../../config/variables");

//models
const { Order } = require("../models/Order");
const { Product } = require("../models/Product");

module.exports = class OrdersController
{
	async addOrder(req, res)
	{
		//creating client
		const http = new HttpClient();

		//preparing items
		let items = [];
		try { items = Array.isArray(req.body["items"]) ? req.body["items"] : JSON.parse(req.body["items"]) } catch (error) {}
		items = Array.isArray(items) ? items.filter(item => typeof item == "object" && !isNaN(parseInt(item.amount)) && mongoose.Types.ObjectId.isValid(item.product)) : [];
		items = items.reduce((items, item) => items.map(item => item.product).includes(item.product) ? items : [...items, item], []); //removing duplicates
		items = await Promise.all(items.map(item => {
			return new Promise(async resolve => {
				const product = await http.request("products","get", "/products/" + item.product).catch(error => console.log("Not found"));
				item.product = product ? product.response : null;
				resolve(item);
			})
		}))
		items = items.filter(item => item.product && item.amount > 0 && item.product.availability >= item.amount);

		//no valid items
		if(!items.length)
			return res.status(404).json(Utils.parseStringError('No valid items', 'items'));

		const order = new Order({
			items: [], //temporary
			user: mongoose.Types.ObjectId(req.user._id),
			payment: mongoose.Types.ObjectId(req.user._id), //temporary
			user_details: {
				email: req.body.email || req.user.email,
				first_name: req.body.first_name || req.user.first_name,
				last_name: req.body.last_name || req.user.last_name,
				phone_number: req.body.phone_number || req.user.phone_number,
				address: {
					postal_code: req.body.postal_code || req.user.address.postal_code,
					address_line_1: req.body.address_line_1  || req.user.address.address_line_1,
					address_line_2: req.body.address_line_2 == '' ? null : req.body.address_line_2 || req.user.address.address_line_2,
					city: req.body.city || req.user.address.city,
					country: req.body.country == '' ? null : req.body.country || req.user.address.country
				},
			},
			total: items.reduce((total, item) => (item.amount * item.product.price) + total, 0)
		})

		order
			.validate()
			.then(() => {

				//creating payment
				http.request("payments", "post", "/payments", {
					"access-token": req.token,
					permissions: ["USER_FULL"],
					amount: order.total,
					token: req.body.token,
					order: order._id.toString()
				})
				.then(async payment => {

					//processing items
					const fileHandler = new FileHandler(req);
					const processedItems = await Promise.all(items.map(item => {
						return new Promise(async resolve => {

							//preparing product
							const product = new Product({
								original_id: item.product._id,
								title: item.product.title,
								category: item.product.category.title,
								manufacturer: item.product.manufacturer,
								intro: item.product.intro,
								description: item.product.description,
								price: item.product.price,
								age_category: item.product.age_category,
								languages: item.product.languages,
								game_modes: item.product.game_modes,
								release_type: item.product.release_type,
								premiere: item.product.premiere
							});

							//downloading image
							const filePath = await fileHandler.saveExternalSourceFile(
								item.product.image.replace("http://undefined/products", variables.microservices.products.url),
								"products/" + product._id
							).catch(error => console.log(error));
							product.image = filePath;

							//saving product
							await product.save().catch(error => console.log(error))
							item.product = product;

							//generating codes
							const codes = [];
							for(let i = 0; i < item.amount; i++)
								codes.push(randomstring.generate({ length: 16, charset: "alphanumeric", capitalization: "uppercase"}));
							item.codes = codes;

							//reduce availability message
							req.app.rabbitmq.publish({ amount: item.amount, id: product.original_id }, { queue: "product_availability", exchange: "product_availability"})

							resolve(item);
						});
					}))

					//saving order
					order.items = processedItems;
					order.payment = mongoose.Types.ObjectId(payment.response._id);
					await order.save().catch(error => console.log(error));

					//sending email
					const data = {
						email: order.user_details.email,
						name: order.user_details.first_name,
						total: order.total,
						items: processedItems.map(item => {
							return {
								title: item.product.title,
								price: item.product.price,
								pieces: item.codes.length
							}
						})
					}
					req.app.rabbitmq.publish({ type: "ORDER_PLACED", data }, { queue: "emails", exchange: "emails"})

					//sending response
					return res.status(201).json({ status: true, _id: order._id });

				})
				.catch(error => res.status(error.status).json(error.response))
			})
			.catch(error => res.status(406).json(Utils.parseValidatorErrors(error)));
	}

	async getOrders(req, res)
	{
		//preparing query
		const { pipeline } = OrdersQueryHandler.ordersListHandle();
		if(req.user.roles.includes("USER"))
			pipeline.unshift({ $match: { user: mongoose.Types.ObjectId(req.user._id) }})

		//filter by product
		if(req.query.product && mongoose.Types.ObjectId.isValid(req.query.product))
			pipeline.unshift({ $match: { "items.product": mongoose.Types.ObjectId(req.query.product) }})

		// sorts
		const sortConfig = {
			created: "created",
			total: "total",
			products_count: "products_count",
			pieces_count: "pieces_count"
		};
		const options = Utils.addSortOption({}, req.query.sort, sortConfig);

		// pagination and parsing
		const paginated = await Utils.paginate(Order, { query: Order.aggregate(pipeline), options: options }, req, true);
		paginated.results.map(order => Order.parse(order, req));

		res.json(paginated);
	}

	async getReviewsHome(req, res)
	{
		const pipeline = OrdersQueryHandler.reviewsList();
		const result = await Order.aggregate(pipeline).exec().catch(error => console.log(error));

		res.json({ reviews: result });
	}

	async getOrder(req, res)
	{
		const order = await Order.findById(req.params.id, {
			"items._id": 1,
			"items.sent": 1,
			"items.codes": 1,
			"items.review.rate": 1,
			"items.review.description": 1,
			"items.review.approved": 1,
			user: 1,
			payment: 1,
			"user_details.email": 1,
			"user_details.first_name": 1,
			"user_details.last_name": 1,
			"user_details.phone_number": 1,
			"user_details.address.postal_code": 1,
			"user_details.address.address_line_1": 1,
			"user_details.address.address_line_2": 1,
			"user_details.address.city": 1,
			"user_details.address.country": 1,
			total: 1,
			created: 1
		})
		.populate({
			path: 'items.product',
			select: { _id: 1, original_id: 1, title: 1, image: 1, price: 1, premiere: 1 }
		})
		.lean()
		.exec()
		.catch(error => console.log("Invalid object id"));

		// not found
		if (!order)
			return res.status(404).json(Utils.parseStringError('Order not found', 'order'));

		//checking permission to authenticated user
		if(!req.user.roles.includes("ADMIN") && req.user._id != order.user.toString())
			return res.status(403).json(Utils.parseStringError('Permission denied', 'permission'));

		//sending response
		const parsed = Order.parse(order, req);
		parsed.items = parsed.items.map(item => {
			if(!req.user.roles.includes("ADMIN") && !item.sent)
				item.codes = [];
			return item;
		})

		res.json(parsed);
	}

	async reviewOrderProduct(req, res)
	{
		//getting order
		const order = await Order.findOne({"items._id": req.params.id, user: req.user._id }).exec().catch(error => console.log("Invalid object id"));

		//not found
		if (!order)
			return res.status(404).json(Utils.parseStringError('Item not found', 'item'));

		//getting item
		const item = order.items.find(item => item._id.toString() == req.params.id);
		if(item.review)
			return res.status(409).json(Utils.parseStringError('This item has already been reviewed', 'review'));

		item.review = {
			rate: req.body.rate,
			description: req.body.description
		};

		//saving order
		order
			.save()
			.then(() => res.status(201).json({ status: true }))
			.catch(error => res.status(406).json(Utils.parseValidatorErrors(error)));
	}

	async changeReviewState(req, res)
	{
		//getting order
		const order = await Order.findOne({"items._id": req.params.id}).populate("items.product").exec().catch(error => console.log("Invalid object id"));

		//not found
		if (!order)
			return res.status(404).json(Utils.parseStringError('Item not found', 'item'));

		//getting item
		const item = order.items.find(item => item._id.toString() == req.params.id);
		if(!item.review)
			return res.status(409).json(Utils.parseStringError('This item is not reviewed', 'review'));

		item.review.approved = !isNaN(parseInt(req.body.approved)) ? parseInt(req.body.approved) : item.review.approved;

		//saving order
		order
			.save()
			.then(async() => {

				//sending response
				res.json({ status: true })

				//updating aggregated rate
				const pipeline = OrdersQueryHandler.calculateProductRate(item.product.original_id);
				const result = (await Order.aggregate(pipeline).exec() || []).pop();
				if(result && result._id)
					req.app.rabbitmq.publish({ rate: result.rate, id: result._id }, { queue: "product_rate", exchange: "product_rate"})
			})
			.catch(error => res.status(406).json(Utils.parseValidatorErrors(error)));
	}
}
