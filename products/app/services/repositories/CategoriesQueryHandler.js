const childrenLookupFunction = (loops = 2) => {
    let lookups = null;
    for (let i = 0; i < loops; i++)
    {
        const nestedLookup = {
            $lookup: {
                from: "categories",
                as: "children",
                let: {foreignId: "$_id"},
                pipeline: [
                    {
                        $match: {$expr: {$eq: ["$parent", "$$foreignId"]}}
                    },
                    {
                        $project: {
                            _id: 1,
                            children: 1,
                        }
                    }
                ]
            }
        }

        if(lookups)
        {
            nestedLookup.$lookup.pipeline.push(lookups);
            nestedLookup.$lookup.pipeline.push({
                $addFields: {
                    childrenTree: { $concatArrays: [{ $map: { input: "$children", as: "child", in: "$$child._id"}}, ["$_id"]]}
                }
            });

        }

        lookups = nestedLookup;
    }
    return lookups;
}

module.exports = {

    categoriesListHandle: (req) => {

        // params
        const search = req.query.search || '';
        const pattern = new RegExp('^.*' + search + '.*$');

        const pipeline = [
            {
                $match: {
                    title: { $regex: pattern, $options: "xi" }
                }
            },
            {
                $lookup: {
                    from: "categories",
                    as: "parent",
                    let: { foreignId: "$parent" },
                    pipeline: [
                        {
                            $match: { $expr: { $eq: ["$_id", "$$foreignId"] } }
                        },
                        {
                            $project: {
                                title: 1,
                                created: 1
                            }
                        }
                    ]
                }
            },
            {
                $unwind: {
                    path: "$parent",
                    preserveNullAndEmptyArrays: true
                }
            },
            childrenLookupFunction(),
            {
                $addFields: {
                    childrenTree: {
                        $reduce: {
                            input: "$children.childrenTree",
                            initialValue: [ "$_id"],
                            in: { $concatArrays: ["$$value", "$$this"]}
                        }
                    }
                }
            },
            {
                $lookup: {
                    from: "products",
                    as: "products",
                    let: { foreignIds: "$childrenTree" },
                    pipeline: [
                        {
                            $match: { $expr: { $in: ["$category", "$$foreignIds"] } }
                        },
                        {
                            $project: {
                                _id: 1
                            }
                        }
                    ]
                }
            },
            {
                $project: {
                    title: 1,
                    products_count: { $size: "$products"},
                    parent: 1,
                    created: 1,
                }
            }
        ];

        return { pipeline };
    },
    prepareCategoriesToParentLookup: (maxLevel = 4) => {

        //preparing parent lookups
        let lookups = null;

        for(let i = 0; i < maxLevel; i++)
        {
            const nestedLookup = {
                $lookup: {
                    from: "categories",
                    as: "parent",
                    let: { foreignId: "$parent" },
                    pipeline: [
                        {
                            $match: {
                                $expr: {
                                    $eq: ["$_id", "$$foreignId" ]
                                }
                            },
                        },
                        {
                            $project: {
                                title: 1,
                                parent: 1,
                                categories_tree: {
                                    $concatArrays: [
                                        {
                                            $cond: {
                                                if: { $and:[ { $isArray: "$parent"}, { $size: "$parent" }]},
                                                then: {
                                                    $arrayElemAt: ["$parent.categories_tree", 0]
                                                },
                                                else: []
                                            }
                                        },
                                        ["$_id"]
                                    ]
                                },
                                created: 1
                            }
                        }
                    ]
                }
            };

            if(lookups)
                nestedLookup.$lookup.pipeline.push(lookups);

            lookups = nestedLookup;
        }

        return { lookups };
    },

    childrenLookup: childrenLookupFunction
};
