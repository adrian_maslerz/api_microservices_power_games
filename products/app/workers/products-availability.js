
//loading config
require('dotenv').config();
require("./../../config/database").load();

//models
const { Product } = require("./../models/Product");

//services
const QueuesHandler = require('./../services/core/QueuesHandler');

//queues config
const config = {
    queue: "product_availability",
    exchange: "product_availability"
};

//Notifications listener
const handler = new QueuesHandler();
handler.subscribe(async data => {

    if(data.amount && data.id)
    {
        const product = await Product.findById(data.id).exec().catch(error => console.log(error));
        if(product)
        {
            product.availability = (product.availability - data.amount);
            await product.save().catch(error => console.log(error))
        }
    }

}, config);
