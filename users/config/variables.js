//loading environment config
const env = process.env;

module.exports = {
	app: {
		name: env.APP_NAME,
		domain: env.APP_DOMAIN,
	},
	database: {
		host: env.DB_HOST || "localhost",
		srv: Boolean(parseInt(env.DB_SRV)),
		port: env.DB_PORT,
		user: env.DB_USER || "",
		password: env.DB_PASSWORD || "",
		database: env.DB_DATABASE
	},
	services: {
		rabbitmq: {
			prefix: (env.RABBITMQ_QUEUE_PREFIX || "base") + "_",
			hostname: env.RABBITMQ_HOSTNAME
		}
	},
	folders: {
		views: __dirname + "/../views",
		uploads: __dirname + "/../public/uploads",
		public: __dirname + "/../public",
	},
	paths: {
		folders: {
			uploads: "/uploads"
		},
		emails: {
			EMAIL_VERIFICATION: "/email-verification?token=:token",
			PASSWORD_RESET: "/password-reset?token=:token",
			PASSWORD_RESET_ADMIN: "/password-reset?token=:token"
		}
	}
}
