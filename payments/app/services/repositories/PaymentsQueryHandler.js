
//models

module.exports = {

    paymentsListHandle: () => {

        const pipeline = [
            {
                $lookup: {
                    from: "customers",
                    as: "customer",
                    let: { foreignId: "$customer" },
                    pipeline: [
                        {
                            $match: { $expr: { $eq: ["$_id", "$$foreignId"] } }
                        },
                        {
                            $project: {
                                stripe_id: 1,
                                user: 1,
                                email: 1,
                                created: 1
                            }
                        }
                    ]
                }
            },
            { $unwind: "$customer" },
            {
                $project: {
                    customer: 1,
                    order: 1,
                    card: 1,
                    currency: 1,
                    description: 1,
                    amount: 1,
                    created: 1
                }
            }
        ];

        return { pipeline };
    }
};
