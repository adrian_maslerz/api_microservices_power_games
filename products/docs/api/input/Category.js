/**
 * @api {post} /categories Add category
 * @apiSampleRequest /categories
 * @apiVersion 0.0.1
 * @apiName Add category
 * @apiGroup Category
 *
 * @apiParam {String} title Category title. Max 100 characters.
 * @apiParam {String} [parent] Parent category id
 *
 * @apiSuccess (Success 201){Boolean} status Operation status.
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 201 OK
 *     {
 *         "status": true
 *     }
 *
 * @apiUse WrongParametersError
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 406 Wrong parameters
 *     {
 *          "errors": [
 *              {
 *                   "field": "title",
 *                   "message": "Title is required"
 *              }
 *          ]
 *      }
 *
 * @apiUse AuthenticateElements
 * @apiUse AuthorizeElements
 */

/**
 * @api {get} /categories Categories list
 * @apiSampleRequest /categories
 * @apiVersion 0.0.1
 * @apiName Categories list
 * @apiGroup Category
 *
 * @apiUse PaginationElements

 * @apiParam {String} [search] Search string.
 * @apiParam {String} [parent] Parent category id.
 * @apiParam {String="createdASC", "createdDESC", "titleASC", "titleDESC", "parentASC", "parentDESC"} [sort="createdDESC"] Sort option.
 *
 * @apiSuccess {Object[]} results Categories list.
 *
 * @apiSuccess {String} results._id Category id
 * @apiSuccess {String} results.title Category title
 * @apiSuccess {Number} results.products_count Category products count.
 * @apiSuccess {Number} results.created Category created date as timestamp ms.
 *
 * @apiSuccess {Object} [results.parent] Category parent object
 * @apiSuccess {String} results.parent._id Category parent id
 * @apiSuccess {String} results.parent.title Category parent title
 * @apiSuccess {Number} results.parent.created Category parent created date as timestamp ms.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *          "results": [
 *              {
 *                  "_id": "5f09e04ad3f63e00ffe1b68f",
 *                  "parent": {
 *                      "_id": "5f09de8454e0c4003b5e4189",
 *                      "created": 1594482308090,
 *                      "title": "PC"
 *                  },
 *                  "created": 1594482762011,
 *                  "products_count": 2,
 *                  "title": "Adventures"
 *              },
 *          ],
 *          "pages": 1,
 *          "total": 3,
 *     }
 **/

/**
 * @api {get} /categories/:id Category details
 * @apiSampleRequest off
 * @apiVersion 0.0.1
 * @apiName Category details
 * @apiGroup Category
 *
 * @apiParam {String} id Category id.
 *
 * @apiSuccess {String} _id Category id
 * @apiSuccess {String} title Category title
 * @apiSuccess {Number} created Category created date as timestamp ms.
 *
 * @apiSuccess {Object} [parent] Category parent object
 * @apiSuccess {String} parent._id Category parent id
 * @apiSuccess {String} parent.title Category parent title
 * @apiSuccess {Number} parent.created Category parent created date as timestamp ms.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *         "_id": "5f09e04ad3f63e00ffe1b68f",
 *         "parent": {
 *             "_id": "5f09de8454e0c4003b5e4189",
 *             "created": 1594482308090,
 *             "title": "PC"
 *         },
 *         "created": 1594482762011,
 *         "title": "Adventures"
 *     }
 *
 * @apiUse NotFoundError
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not found
 *     {
 *          "errors": [
 *              {
 *                   "field": "category",
 *                   "message": "Category not found"
 *              }
 *          ]
 *      }
 **/

/**
 * @api {put} /categories/:id Update category
 * @apiSampleRequest off
 * @apiVersion 0.0.1
 * @apiName Update category
 * @apiGroup Category
 *
 * @apiParam {String} id Category id.
 * @apiParam {String} [title] Category title. Max 100 characters.
 * @apiParam {String} [parent] Parent category id
 *
 * @apiUse SuccessOperationStatus
 *
 * @apiUse NotFoundError
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not found
 *     {
 *          "errors": [
 *              {
 *                   "field": "category",
 *                   "message": "Category not found"
 *              }
 *          ]
 *      }
 * @apiUse WrongParametersError
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 406 Wrong parameters
 *     {
 *          "errors": [
 *              {
 *                   "field": "title",
 *                   "message": "Title is required"
 *              }
 *          ]
 *      }
 * @apiUse AuthenticateElements
 * @apiUse AuthorizeElements
 **/

/**
 * @api {delete} /categories/:id Delete category
 * @apiSampleRequest off
 * @apiVersion 0.0.1
 * @apiName Delete category
 * @apiGroup Category
 *
 * @apiParam {String} id Category id.
 *
 * @apiUse SuccessOperationStatus
 *
 * @apiUse NotFoundError
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not found
 *     {
 *          "errors": [
 *              {
 *                   "field": "category",
 *                   "message": "Category not found"
 *              }
 *          ]
 *      }
 *
 * @apiUse ConflictError
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 409 Conflict
 *     {
 *          "errors": [
 *              {
 *                   "field": "product",
 *                   "message": "Cannot delete category associated to products"
 *              }
 *          ]
 *      }
 * @apiUse AuthenticateElements
 * @apiUse AuthorizeElements
 **/
