
//services
const Utils = require('../services/utilities');
const StripeHandler = require('../services/feature/StripeHandler');

//models
const { Customer } = require("../models/Customer");

module.exports = class CardsController
{
	async deleteCustomerCard(req, res)
	{
		//getting customer
		const customer = await Customer.findOne({ user: req.user._id, "cards._id": req.params.id }).exec().catch(error => console.log("Invalid object id"))
		if(!customer)
			return res.status(404).json(Utils.parseStringError('Card not found', 'card'));

		const card = customer.cards.find(card => card._id.toString() == req.params.id);

		//deleting from stripe
		const handler = new StripeHandler();
		await handler.deleteCard(customer, card).catch(error => console.log(error))

		//deleting from local
		customer.cards.pull(card);
		await customer.save().catch(error => console.log(error))

		//sending response
		res.json({ status: true })
	}
}
