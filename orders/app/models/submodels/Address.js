// modules
const mongoose = require('mongoose');

// schema
const schema = mongoose.Schema({
    postal_code: {
        type: String,
        required: [true, '{PATH} is required.'],
        trim: true,
        maxlength: [30, "{PATH} can't be longer than {MAXLENGTH} characters."]
    },
    address_line_1: {
        type: String,
        required: [true, '{PATH} is required.'],
        trim: true,
        maxlength: [50, "{PATH} can't be longer than {MAXLENGTH} characters."]
    },
    address_line_2: {
        type: String,
        trim: true,
        maxlength: [50, "{PATH} can't be longer than {MAXLENGTH} characters."],
        default: null
    },
    city: {
        type: String,
        required: [true, '{PATH} is required.'],
        trim: true,
        maxlength: [50, "{PATH} can't be longer than {MAXLENGTH} characters."]
    },
    country: {
        type: String,
        trim: true,
        maxlength: [50, "{PATH} can't be longer than {MAXLENGTH} characters."],
        default: null
    },
});

module.exports = { Address: schema };
