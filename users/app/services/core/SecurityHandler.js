// core
const JWT = require('jsonwebtoken');

// custom
const { roles } = require('../../../config/access-control');
const { User } = require('../../models/User');

module.exports = {

	authenticate: (token) => {

		return new Promise(async (resolve, reject) => {

			// token not found
			if (!token)
				return reject({ error: 'Access Denied', field: 'user', status: 401 });

			// user not found
			const user = await User.findOne({ 'auth.token': token }).exec().catch(error => console.log('Not found'));
			if (!user)
				return reject({ error: 'Access Denied', field: 'user', status: 401 });

			// token verification
			const auth = user.auth.find(auth => auth.token == token);
			verifyToken(token, auth.signature_key)
				.then(() => resolve(user))
				.catch(error => reject(error));
		});
	},

	verifyPermissions: (user, allowedPermissions) => {

		let isPermitted = false;

		// permissions handle
		user.permissions.forEach(permission => {
			if (allowedPermissions.includes(permission))
				isPermitted = true;
		});

		// role permissions handle
		user.roles.forEach(role => {
			const configRole = roles.find(configRole => configRole.role === role);
			const rolePermissions = configRole ? configRole.permissions : [];

			rolePermissions.forEach(permission => {
				if (allowedPermissions.includes(permission))
					isPermitted = true;
			});
		});

		return isPermitted;
	}
};

async function verifyToken (token, signature) {

	return new Promise((resolve, reject) => {
		JWT.verify(token, signature, (error) => {

			// error handle
			if (error)
			{
				if (error.name === 'TokenExpiredError')
					return reject({ error: 'Access token expired', field: 'user', status: 410 });

				return reject({ error: 'Access Denied', field: 'user', status: 401 });
			}

			return resolve();
		});
	});
}

