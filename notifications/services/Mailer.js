
const sendgrid = require('@sendgrid/mail');
const variables = require("./../variables");

module.exports.load = function(app)
{
    app.mailer = prepareMailer();
}

module.exports.getMailer = function()
{
    return prepareMailer();
}

function prepareMailer()
{
    sendgrid.setApiKey(variables.mailer.sendgrid.key);

    return {
        send: function(to, subject, templateName, name, args)
        {
            const params = {
                to: to,
                from: variables.mailer.from,
                subject: subject,
                templateId: variables.mailer.sendgrid.templates[templateName],
                personalizations:[{
                    to:[
                        {
                            email: to,
                            name: name
                        }
                    ],
                    dynamic_template_data: args
                }],
            }
            sendgrid.send([ params ]).catch(e => console.log(e));
        }
    }
}

