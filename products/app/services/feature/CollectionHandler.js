
module.exports = {
    prepareCollection: (input, types) => {
        let collection = [];
        try { collection = Array.isArray(input) ? input : JSON.parse(input) } catch (error) {}
        return Array.isArray(collection) ? collection.filter(item => typeof item == "string" &&  Object.values(types).includes(item)) : [];
    }
}

