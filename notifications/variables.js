//loading environment config
const env = process.env;

module.exports = {
	app: {
		name: env.APP_NAME,
		app_domain: env.APP_DOMAIN,
		admin_domain: env.ADMIN_DOMAIN,
		contact_mail: env.CONTACT_EMAIL
	},
	mailer: {
		from: env.MAIL_FROM + "<" + env.MAIL_FROM_ADDRESS + ">",
		sendgrid: {
			key: env.SENDGRID_API_KEY,
			templates: {
				"forgot-password": env.SENDGRID_TEMPLATE_FORGOT_PASSWORD,
				"account-created": env.SENDGRID_TEMPLATE_ACCOUNT_CREATED,
				"order-placed": env.SENDGRID_TEMPLATE_ORDER_PLACED,
				"game-codes": env.SENDGRID_TEMPLATE_GAME_CODES,
				"contact-message": env.SENDGRID_TEMPLATE_CONTACT_MESSAGE,
			}
		}
	},
    services: {
		rabbitmq: {
			prefix: (env.RABBITMQ_QUEUE_PREFIX || "base") + "_",
			hostname: env.RABBITMQ_HOSTNAME
		},
    },
	paths: {
		emails: {
			PASSWORD_RESET: "/password-reset?token=:token"
		}
	}
}
