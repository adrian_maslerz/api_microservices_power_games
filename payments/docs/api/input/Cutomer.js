
/**
 * @api {get} /customers/:id Customer details
 * @apiSampleRequest off
 * @apiVersion 0.0.1
 * @apiName Customer details
 * @apiGroup Customer
 *
 * @apiParam {String} id User id.
 *
 * @apiSuccess {String} _id Customer id
 * @apiSuccess {Number} created Customer created date as timestamp ms.
 * @apiSuccess {String} email Customer email
 * @apiSuccess {String} stripe_id Customer stripe id
 * @apiSuccess {String} user User id
 *
 * @apiSuccess {Object[]} cards Cards objects
 * @apiSuccess {String} cards._id Card id
 * @apiSuccess {String} cards.card_id Card id from stripe
 * @apiSuccess {String} cards.brand Card brand
 * @apiSuccess {String} cards.last_4 Card last 4
 * @apiSuccess {String} cards.exp_month Card expiration month
 * @apiSuccess {String} cards.exp_year Card expiration year
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *          "_id": "5f0e2b6b5ea2070180507458",
 *          "created": 1594764139935,
 *          "stripe_id": "cus_HeEgew5AczOecB",
 *          "email": "test@test.com",
 *          "user": "5f0e2b6b5ea2070180507458",
 *          "cards": [
 *              {
 *                  "_id": "5f0f706272627300f13eef4f",
 *                  "card_id": "card_1H5HqRIs9F98ahEkTBi9oVXf",
 *                  "brand": "Visa",
 *                  "last_4": "4242",
 *                  "exp_month": "7",
 *                  "exp_year": "2021"
 *              }
 *          ]
 *     }
 *
 * @apiUse NotFoundError
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not found
 *     {
 *          "errors": [
 *              {
 *                   "field": "customer",
 *                   "message": "Customer not found"
 *              }
 *          ]
 *      }
 * @apiUse AuthenticateElements
 * @apiUse AuthorizeElements
 **/
