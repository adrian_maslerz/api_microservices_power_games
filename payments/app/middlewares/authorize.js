/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//services
const Utils = require('../services/utilities');
const HttpClient = require("./../services/core/HttpClient");

module.exports = function (permissions = [])
{
    return function (req, res, next)
    {
        //getting token
        const token = req.headers['x-access-token'] || req.query["access-token"] || req.body["access-token"];
        const client = new HttpClient();
        client
            .request("users", "post", "/authorize", { "access-token": token, permissions })
            .then(response => {
                req.user = response.response;
                next();
            })
            .catch(error => res.status(error.status).json(error.response))
    }
}
    
        

    
