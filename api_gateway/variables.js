//loading environment config
const env = process.env;

module.exports = {
	app: {
		name: env.APP_NAME,
		port: env.PORT
	},
	microservices: {
		users: {
			url: env.MICROSERVICE_USERS_URL,
			prefix: "/users"
		},
		products: {
			url: env.MICROSERVICE_PRODUCTS_URL,
			prefix: "/products"
		},
		payments: {
			url: env.MICROSERVICE_PAYMENTS_URL,
			prefix: "/payments"
		},
		orders: {
			url: env.MICROSERVICE_ORDERS_URL,
			prefix: "/orders"
		}
	}
}
