//core
const mongoose = require("mongoose");

//services
const Utils = require('../services/utilities');
const StripeHandler = require('../services/feature/StripeHandler');
const PaymentsQueryHandler = require('../services/repositories/PaymentsQueryHandler');

//models
const { Customer } = require("../models/Customer");
const { Payment } = require("../models/Payment");

module.exports = class PaymentsController
{
	async addPayment(req, res)
	{
		//input validation
		let errors;
		req.check("token").notEmpty().withMessage('Token is required.');
		req.check("amount")
			.notEmpty()
			.withMessage('Amount is required.')
			.custom(value => {
				const amount = !isNaN(parseFloat(value)) ? parseFloat(value) : 0;
				return amount > 2;
			})
			.withMessage('Amount must be greater than 2');
		req.check("order")
			.notEmpty().withMessage('Order is required.')
			.custom(value => mongoose.Types.ObjectId.isValid(value))
			.withMessage('This is not valid order id');

		if(errors = req.validationErrors())
			return res.status(406).json({ errors: errors });


		//creating handler
		const handler = new StripeHandler();

		//getting / preparing customer
		let customer = await Customer.findOne({ user: req.user._id }, ).exec().catch(error => console.log("Invalid object id"));
		if(!customer)
		{
			//creating customer on stripe
			const customerData = await handler.createCustomer(req.user).catch(error => console.log(error));
			if(!customerData)
				return res.status(406).json(Utils.parseStringError('Invalid customer data', 'customer'));

			//creating customer in local DB
			customer = new Customer({
				stripe_id: customerData.id,
				user: req.user._id,
				email: req.user.email
			});

			customer = await customer.save().catch(error => console.log(error));
			if(!customer)
				return res.status(406).json(Utils.parseStringError('Invalid customer data', 'customer'));
		}

		//checking payment
		const payment = await Payment.findOne({ customer: customer, order: req.body.order }).exec().catch(error => console.log(error));
		if(payment)
			return res.status(409).json(Utils.parseStringError('Payment already exist', 'payment'));

		//preparing card
		let card = null;
		if(/^card/.test(req.body.token))
		{
			card = customer.cards.find(card => card.card_id == req.body.token)
			if(!card)
				return res.status(406).json(Utils.parseStringError('Invalid card id', 'card'));
		}
		else
		{
			//handling new card
			const cardData = await handler.createCard(customer, req.body.token).catch(error => console.log(error));
			if(!cardData)
				return res.status(406).json(Utils.parseStringError('Invalid card', 'card'));

			customer.cards.push({
				card_id: cardData.id,
				brand: cardData.brand,
				last_4: cardData.last4,
				exp_month: cardData.exp_month,
				exp_year: cardData.exp_year
			})

			await customer.save().catch(error => console.log(error))
			card = customer.cards.find(card => card.card_id == cardData.id);
		}

		//creating charge
		handler.createCharge(req.body.token, parseFloat(req.body.amount), customer, req.body.order)
			.then(async charge => {

				//creating payment
				const payment = new Payment({
					customer: customer,
					order: mongoose.Types.ObjectId(req.body.order),
					card: card,
					currency: charge.currency,
					description: charge.description,
					amount: parseFloat(req.body.amount)
				})

				await payment.save().catch(error => console.log(error));

				//sending response
				return res.status(201).json({ status: true, _id: payment._id });
			})
			.catch(error => res.status(406).json(Utils.parseStringError(error.message, error.param )));
	}

	async getPayments(req, res)
	{
		//preparing query
		const { pipeline } = PaymentsQueryHandler.paymentsListHandle(req);

		// pagination and parsing
		const paginated = await Utils.paginate(Payment, { query: Payment.aggregate(pipeline), options: { sortBy: { created: -1, _id: 1}} }, req, true);
		paginated.results.map(payment => Payment.parse(payment, req));

		res.json(paginated);
	}

	async getCustomerPayments(req, res)
	{
		//checking permission to authenticated user
		if(req.user._id != req.params.id)
			return res.status(403).json(Utils.parseStringError('Permission denied', 'permission'));

		//getting customer
		const customer = await Customer.findOne({ user: req.user._id }).exec().catch(error => console.log("Invalid object id"))

		//not found
		if(!customer)
			return res.status(404).json(Utils.parseStringError('Customer not found', 'customer'));

		//preparing query
		const { pipeline } = PaymentsQueryHandler.paymentsListHandle(req);
		pipeline.unshift({ $match: { customer: customer._id }});


		// pagination and parsing
		const paginated = await Utils.paginate(Payment, { query: Payment.aggregate(pipeline), options: { sortBy: { created: -1, _id: 1}} }, req, true);
		paginated.results.map(payment => Payment.parse(payment, req));

		res.json(paginated);
	}

	async getPayment(req, res)
	{
		const payment = await Payment.findById(req.params.id, {
			customer: 1,
			card: 1,
			order: 1,
			currency: 1,
			description: 1,
			amount: 1,
			created: 1
		})
		.populate({
			path: "customer",
			select: {
				stripe_id: 1,
				user: 1,
				email: 1,
				created: 1
			}
		})
		.lean()
		.exec()
		.catch(error => console.log("Invalid object id"));

		//not found
		if(!payment)
			return res.status(404).json(Utils.parseStringError('Payment not found', 'payment'));

		//checking permission to authenticated user
		if(!req.user.roles.includes("ADMIN") && req.user._id != payment.customer.user.toString())
			return res.status(403).json(Utils.parseStringError('Permission denied', 'permission'));

		//sending response
		res.json(Payment.parse(payment, req));
	}
}
