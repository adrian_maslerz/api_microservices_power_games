//core
const axios = require('axios');

//config
const variables = require("./../../../config/variables")

module.exports = class HttpClient
{
    constructor()
    {
        this.client = axios;
    }

    request(microservice, method, path, data)
    {
        return new Promise((resolve, reject) => {
            this.client.request({
                method: method,
                url: Object.keys(variables.microservices).includes(microservice) ? variables.microservices[microservice].url + variables.microservices[microservice].prefix + path : "",
                data: data
            })
            .then(response => resolve({ response: response.data, status: response.status }))
            .catch(error => reject({ response:error.response ? error.response.data : null, status: error.response ? error.response.status : 500 }));
        });
    }
}
