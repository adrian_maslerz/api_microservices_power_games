// core
const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');
const mongooseAggregatePaginate = require('mongoose-aggregate-paginate');
const uniqueValidator = require('mongoose-unique-validator');

const validators = require('../services/validators');

// schema
const schema = mongoose.Schema({
    stripe_id: {
        type: String,
        required: [true, '{PATH} is required.'],
        unique: true
    },
    user: {
        type: mongoose.Schema.Types.ObjectId,
        required: [true, '{PATH} is required.'],
        unique: true
    },
    email: {
        type: String,
        required: [true, '{PATH} is required.'],
        validate: validators.email,
        trim: true,
        lowercase: true,
        unique: true
    },
    cards: [
        {
            card_id: {
                type: String,
                required: [true, '{PATH} is required.'],
            },
            brand: {
                type: String,
                required: [true, '{PATH} is required.'],
            },
            last_4: {
                type: String,
                required: [true, '{PATH} is required.'],
            },
            exp_month: {
                type: String,
                required: [true, '{PATH} is required.'],
            },
            exp_year: {
                type: String,
                required: [true, '{PATH} is required.'],
            }
        }
    ],
    created: {
        type: Date,
        default: Date.now()
    },
    updated: {
        type: Date,
        default: Date.now()
    }
}, { usePushEach: true });

// middlewares
schema.pre('save', function (next) {

    // dates handle
    const customer = this;
    if (customer.isNew)
        customer.created = new Date();
    if (customer.isModified())
        customer.updated = new Date();

    next();
});

// statics
schema.statics.parse = function (customer, req)
{
    //dates
    if (customer.created)
        customer.created = customer.created.getTime();


    return customer;
};

schema.plugin(uniqueValidator, {message: 'The {PATH} has already been taken.'});
schema.plugin(mongoosePaginate);
schema.plugin(mongooseAggregatePaginate);

module.exports = { Customer: mongoose.model('Customer', schema) };
