// core
const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');
const mongooseAggregatePaginate = require('mongoose-aggregate-paginate');

// schema
const schema = mongoose.Schema({
    title: {
        type: String,
        required: [true, '{PATH} is required.'],
        trim: true,
        maxlength: [100, "{PATH} can't be longer than {MAXLENGTH} characters."]
    },
    parent: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Category',
        default: null
    },
    created: {
        type: Date,
        default: Date.now()
    },
    updated: {
        type: Date,
        default: Date.now()
    }
}, { usePushEach: true });

// middlewares
schema.pre('save', function (next) {

    // dates handle
    const category = this;
    if (category.isNew)
        category.created = new Date();
    if (category.isModified())
        category.updated = new Date();

    next();
});

// statics
schema.statics.parse = function (category, req)
{
    //dates
    if (category.created)
        category.created = category.created.getTime();

    //parent
    if(category.parent)
        category.parent = this.parse(category.parent, req)

    return category;
};

schema.plugin(mongoosePaginate);
schema.plugin(mongooseAggregatePaginate);

module.exports = { Category: mongoose.model('Category', schema) };
