exports.up = async function (next) {
  const admin = new this.model('User').User({
    email: 'admin@admin.com',
    password: 'Qwerty123$%^',
    first_name: 'Admin',
    last_name: 'Admin',
    roles: ['ADMIN'],
    status: 'ACTIVE'
  });

  await admin.hashPassword();
  await admin.save().catch(error => console.log(error));

  next();
};

exports.down = function (next) {
  next();
};
